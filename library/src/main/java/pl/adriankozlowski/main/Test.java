package pl.adriankozlowski.main;

import com.microsoft.azure.storage.blob.CloudBlockBlob;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import pl.adriankozlowski.response.InitResponse;
import xades4j.properties.QualifyingProperty;
import xades4j.properties.SignatureTimeStampProperty;
import xades4j.properties.SigningCertificateProperty;
import xades4j.properties.SigningTimeProperty;
import xades4j.providers.CertificateValidationProvider;
import xades4j.providers.impl.PKIXCertificateValidationProvider;
import xades4j.utils.DOMHelper;
import xades4j.utils.FileSystemDirectoryCertStore;
import xades4j.verification.XAdESVerificationResult;
import xades4j.verification.XadesVerificationProfile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.net.URI;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Collection;

class Test {
    public static final String SCHEMA_VERSION = "1-0";
    public static final String AES_SCHEMA = "AES/CBC/PKCS5Padding";
    public static final String VERSION = "01.02.01.20160617";
    public static final String RSA_ENCODING = "Base64";
    public static final String RSA = "RSA";
    public static final String RSA_MODE = "ECB";
    public static final String RSA_PADDING = "PKCS#1";
    public static final int BLOCK = 16;
    public static final String AES_MODE = "CBC";
    public static final String AES_PADDING = "PKC#7";
    public static final int AES_SIZE = 256;
    public static final String IV_BYTES = "16";
    public static final String ENCODING = "Base64";
    public static final String ALG_MD5 = "MD5";
    public static final String ALG_SHA_256 = "SHA-256";


    private static void sendBlobs(InitResponse initResponse) {
        try {

            for (InitResponse.UploadFile uploadFile : initResponse.getRequestToUploadFileList()) {
                URI url = new URI(uploadFile.getUrl());
                String query = url.getQuery();
                CloudBlockBlob blob = new CloudBlockBlob(url);
                File source = new File(uploadFile.getFileName());
                blob.upload(new FileInputStream(source), source.length());
            }
        }
        // Create or overwrite the "myimage.jpg" blob with contents from a local file.
        catch (Exception e) {
            // Output the stack trace.
            e.printStackTrace();
        }
    }

    private static void verifyBes(File signedXml) throws Exception {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new FileReader(signedXml)));
        DOMHelper.useIdAsXmlId(doc.getDocumentElement());

        NodeList nl = doc.getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", "Signature");

        FileSystemDirectoryCertStore certStore = new FileSystemDirectoryCertStore(new File(Test.class.getResource("/marcincert.pfx").getPath()).getParent());
        KeyStore ks;
        try (FileInputStream fis = new FileInputStream(new File(Test.class.getResource("/marcincert.pfx").getPath()))) {
            ks = KeyStore.getInstance("jks");
            ks.load(fis, "test".toCharArray());
        }

        CertificateValidationProvider provider = new PKIXCertificateValidationProvider(
                ks, false, certStore.getStore());
        XadesVerificationProfile profile = new XadesVerificationProfile(provider);
        Element sigElem = (Element) nl.item(0);
        XAdESVerificationResult r = profile.newVerifier().verify(sigElem, null);

        System.out.println("Signature form: " + r.getSignatureForm());
        System.out.println("Algorithm URI: " + r.getSignatureAlgorithmUri());
        System.out.println("Signed objects: " + r.getSignedDataObjects().size());
        System.out.println("Qualifying properties: " + r.getQualifyingProperties().all().size());

        for (QualifyingProperty qp : r.getQualifyingProperties().all()) {
            if ("SigningCertificate".equals(qp.getName())) {
                Collection<X509Certificate> certs = ((SigningCertificateProperty) qp).getsigningCertificateChain();
                certs.forEach((cert) -> {
                    System.out.println("Issuer DN: " + cert.getIssuerDN());
                });
            } else if ("SigningTime".equals(qp.getName())) {
                System.out.println("Time: " + ((SigningTimeProperty) qp).getSigningTime().getTime().toString());
            } else if ("SignatureTimeStamp".equals(qp.getName())) {
                System.out.println("Time stamp: " + ((SignatureTimeStampProperty) qp).getTime().toString());
            } else {
                System.out.println("QP: " + qp.getName());
            }
        }
    }

}
