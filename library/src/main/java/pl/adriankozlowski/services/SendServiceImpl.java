package pl.adriankozlowski.services;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;
import pl.adriankozlowski.config.Registry;
import pl.adriankozlowski.enums.FileType;
import pl.adriankozlowski.enums.FormCodeType;
import pl.adriankozlowski.exceptions.CipherException;
import pl.adriankozlowski.exceptions.MF400Exception;
import pl.adriankozlowski.exceptions.MF500Exception;
import pl.adriankozlowski.exceptions.MFUnknownException;
import pl.adriankozlowski.remote.SendService;
import pl.adriankozlowski.response.InitResponse;
import xades4j.XAdES4jException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class SendServiceImpl implements SendService {
    public static final String INIT_REQUEST_URL = "initRequestUrl";
    private InitRequestServiceImpl initRequestService = new InitRequestServiceImpl();

    @Override
    public InitResponse composeInitRequest(String pathToJpkXml) throws CipherException, IOException {
        try {
            File file = new File(pathToJpkXml);
            File requestXml = initRequestService.createRequestXml(file, FileType.JPK, FormCodeType.JPK_VAT);
            File signedFile = initRequestService.signXades(requestXml);
            return readJSON(sendHttpRequest(signedFile, Registry.getInstance().getProperty(INIT_REQUEST_URL)));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | KeyStoreException
                | IllegalBlockSizeException | CertificateException | TransformerException | XAdES4jException | ParserConfigurationException | SAXException e) {
            throw new CipherException(e.getMessage(), e);
        } catch (IOException e) {
            throw e;
        } catch (ParseException e) {
            throw new RuntimeException(e.getMessage(), e);
        } catch (JAXBException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * @param signedFile
     * @param urlMF
     * @return
     */
    private String sendHttpRequest(File signedFile, String urlMF) throws IOException {
        URL url = new URL(urlMF);
        HttpsURLConnection request = (HttpsURLConnection) url.openConnection();
        request.setRequestMethod("POST");
        OutputStream outputStream = request.getOutputStream();
        InputStream inputStream = request.getInputStream();
        outputStream.write(Files.readAllBytes(signedFile.toPath()));
        request.connect();
        //sprawdzamy odpowiedź
        int responseCode = request.getResponseCode();
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        if (responseCode == 200) { //jeżeli ok TO:
            return stringBuilder.toString();
        } else if (responseCode > 400 && responseCode < 500) {
            throw new MF400Exception(stringBuilder.toString());
        } else if (responseCode >= 500) {
            throw new MF500Exception(stringBuilder.toString());
        } else {
            throw new MFUnknownException(stringBuilder.toString());
        }
    }

    /**
     * @param json
     * @return
     * @throws IOException
     * @throws ParseException
     */
    private InitResponse readJSON(String json) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(json);
        InitResponse initResponse = new InitResponse();
        JSONObject jsonObject = (JSONObject) obj;
        initResponse.setReferenceNumber((String) jsonObject.get("ReferenceNumber"));
        initResponse.setTimeoutInSec((long) jsonObject.get("TimeoutInSec"));
        JSONArray uploadFiles = (JSONArray) jsonObject.get("RequestToUploadFileList");
        Iterator<JSONObject> iterator = uploadFiles.iterator();
        initResponse.setRequestToUploadFileList(new ArrayList<>());
        while (iterator.hasNext()) {
            InitResponse.UploadFile uploadFile = new InitResponse.UploadFile();
            JSONObject next = (JSONObject) iterator.next();
            uploadFile.setBlobName((String) next.get("BlobName"));
            uploadFile.setFileName((String) next.get("FileName"));
            uploadFile.setUrl((String) next.get("Url"));
            uploadFile.setMethod((String) next.get("Method"));
            JSONArray headers = (JSONArray) next.get("HeaderList");
            HashMap<String, String> headerMap = new HashMap<>();
            uploadFile.setHeaders(headerMap);
            initResponse.getRequestToUploadFileList().add(uploadFile);
            Iterator headersIterator = headers.iterator();
            while (headersIterator.hasNext()) {
                JSONObject header = (JSONObject) headersIterator.next();
                headerMap.put((String) header.get("Key"), (String) header.get("Value"));
            }
        }
        return initResponse;
    }

    @Override
    public void checkStatus() {

    }
}
