package pl.adriankozlowski.services;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import pl.adriankozlowski.config.Registry;
import pl.adriankozlowski.enums.FileType;
import pl.adriankozlowski.enums.FormCodeType;
import pl.adriankozlowski.jpk.*;
import xades4j.XAdES4jException;
import xades4j.algorithms.EnvelopedSignatureTransform;
import xades4j.production.*;
import xades4j.properties.DataObjectDesc;
import xades4j.providers.KeyingDataProvider;
import xades4j.providers.impl.FileSystemKeyStoreKeyingDataProvider;
import xades4j.utils.DOMHelper;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class InitRequestServiceImpl {

    public static final String SCHEMA_VERSION = Registry.getInstance().getProperty("schema_version");
    public static final String AES_SCHEMA = Registry.getInstance().getProperty("aes_schema");
    public static final String RSA_SCHEMA = Registry.getInstance().getProperty("rsa_schema");
    public static final String VERSION = Registry.getInstance().getProperty("version");
    public static final String RSA = Registry.getInstance().getProperty("rsa");
    public static final String RSA_MODE = Registry.getInstance().getProperty("rsa_mode");
    public static final String RSA_PADDING = Registry.getInstance().getProperty("rsa_padding");
    public static final int BLOCK = Integer.valueOf(Registry.getInstance().getProperty("block"));
    public static final String AES_MODE = Registry.getInstance().getProperty("aes_mode");
    public static final String AES_PADDING = Registry.getInstance().getProperty("aes_padding");
    public static final int AES_SIZE = Integer.valueOf(Registry.getInstance().getProperty("aes_size"));
    public static final String IV_BYTES = Registry.getInstance().getProperty("iv_bytes");
    public static final String ENCODING = Registry.getInstance().getProperty("encoding");
    public static final String ALG_MD5 = Registry.getInstance().getProperty("alg_md5");
    public static final String SHA_256 = Registry.getInstance().getProperty("sha_256");
    public static final String ZIP_MODE = Registry.getInstance().getProperty("zip_mode");
    public static final String ZIP_TYPE = Registry.getInstance().getProperty("zip_type");
    public static final String PACKAGE = Registry.getInstance().getProperty("package");
    public static final String DATE_FORMAT = Registry.getInstance().getProperty("date_format");
    public static final String XML_EXT = Registry.getInstance().getProperty("xml_ext");
    public static final String UTF_8 = Registry.getInstance().getProperty("utf_8");
    public static final String AES = Registry.getInstance().getProperty("aes");
    public static final String MD5 = Registry.getInstance().getProperty("md5");

    /**
     * @param jpkFile
     * @param fileType
     * @param formCodeType
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws KeyStoreException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws CertificateException
     */
    public File createRequestXml(File jpkFile, FileType fileType, FormCodeType formCodeType)
            throws NoSuchAlgorithmException, IOException, NoSuchPaddingException, InvalidKeyException, KeyStoreException, BadPaddingException, IllegalBlockSizeException, CertificateException, JAXBException {
        String jpkXmlFileName = jpkFile.getName();
        String sha256XmlHash = getFileSHAHashInBase64(jpkFile);
        byte[] aesEncryptionKey = getAESEncryptionKey();
        String aesKeyEncyptedByRsa = rsaEncrypt(aesEncryptionKey);
        File zipXmlFile = zipXmlFile(jpkFile);
        byte[] initializationVector = new byte[16];
        Path encryptedZipFile = encrypt(aesEncryptionKey, Files.readAllBytes(zipXmlFile.toPath()), initializationVector);
        String ivBase64 = Base64.getEncoder().encodeToString(initializationVector);

        ObjectFactory objectFactory = new ObjectFactory();
        InitUploadType.EncryptionKey encryptionKey = objectFactory.createInitUploadTypeEncryptionKey();
        InitUploadType initUpload = objectFactory.createInitUploadType();
        ArrayOfDocumentType.Document document = objectFactory.createArrayOfDocumentTypeDocument();
        ArrayOfDocumentType documentList = objectFactory.createArrayOfDocumentType();
        DocumentType.FormCode formCode = objectFactory.createDocumentTypeFormCode();
        DocumentType.FileSignatureList fileSignatureList = objectFactory.createDocumentTypeFileSignatureList();
        ArrayOfFileSignatureType.Encryption signatureEncryption = objectFactory.createArrayOfFileSignatureTypeEncryption();
        ArrayOfFileSignatureType.Encryption.AES aes = objectFactory.createArrayOfFileSignatureTypeEncryptionAES();
        ArrayOfFileSignatureType.Encryption.AES.IV iv = objectFactory.createArrayOfFileSignatureTypeEncryptionAESIV();
        ArrayOfFileSignatureType.Packaging packaging = objectFactory.createArrayOfFileSignatureTypePackaging();
        ArrayOfFileSignatureType.Packaging.SplitZip zip = objectFactory.createArrayOfFileSignatureTypePackagingSplitZip();
        FileSignatureType fileSignature = objectFactory.createFileSignatureType();
        FileSignatureType.HashValue hashValue = objectFactory.createFileSignatureTypeHashValue();
        DocumentType.HashValue hash = objectFactory.createDocumentTypeHashValue();

        initUpload.setDocumentType(fileType.toString());
        initUpload.setVersion(VERSION);

        encryptionKey.setAlgorithm(RSA);
        encryptionKey.setEncoding(ENCODING);
        encryptionKey.setMode(RSA_MODE);
        encryptionKey.setPadding(RSA_PADDING);
        encryptionKey.setValue(aesKeyEncyptedByRsa);

        initUpload.setEncryptionKey(encryptionKey);

        formCode.setSchemaVersion(SCHEMA_VERSION);
        formCode.setSystemCode(formCodeType.toString() + " (1)"); //todo figure out  (1)
        formCode.setValue(formCodeType.toString());
        document.setFormCode(formCode);
        document.setFileName(jpkXmlFileName);
        document.setContentLength(jpkFile.length());
        hash.setAlgorithm(SHA_256);
        hash.setEncoding(ENCODING);
        hash.setValue(sha256XmlHash);
        document.setHashValue(hash);
        documentList.setDocument(document);

        aes.setBlock(BLOCK);
        aes.setMode(AES_MODE);
        aes.setPadding(AES_PADDING);
        aes.setSize(AES_SIZE);
        iv.setBytes(IV_BYTES);
        iv.setEncoding(ENCODING);
        iv.setValue(ivBase64);
        aes.setIV(iv);
        signatureEncryption.setAES(aes);
        fileSignatureList.setEncryption(signatureEncryption);
        fileSignatureList.setFilesNumber(1); //todo calculate filenumbers
        document.setFileSignatureList(fileSignatureList);

        zip.setMode(ZIP_MODE);
        zip.setType(ZIP_TYPE);
        packaging.setSplitZip(zip);
        fileSignatureList.setPackaging(packaging);

        fileSignature.setOrdinalNumber(1);//todo :calculate
        fileSignature.setContentLength(1);//todo: calculate
        fileSignature.setFileName(encryptedZipFile.getFileName().toString());
        hashValue.setAlgorithm(ALG_MD5);
        hashValue.setEncoding(ENCODING);

        byte[] fileBytes = Files.readAllBytes(encryptedZipFile);
        hashValue.setValue(getMD5EncodedString(fileBytes));
        fileSignature.setHashValue(hashValue);

        fileSignatureList.getFileSignature().add(fileSignature);

        initUpload.setDocumentList(documentList);
        JAXBElement<InitUploadType> init = objectFactory.createInitUpload(initUpload);
        JAXBContext jc = JAXBContext.newInstance(PACKAGE);
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);

        File outputXmlRequest = new File(formatter.format(new Date()) + XML_EXT);
        marshaller.marshal(init, outputXmlRequest);
        return outputXmlRequest;

    }

    /**
     * @param data
     * @param path
     * @return
     * @throws IOException
     */
    private Path saveFile(byte[] data, String path) throws IOException {
        Path thePath = Paths.get(path);
        if (Files.exists(thePath)) {
            Files.delete(thePath);
        }
        return Files.write(thePath, data, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
    }

    /**
     * @param key
     * @param value
     * @param initVector
     * @return
     */
    private Path encrypt(byte[] key, byte[] value, byte[] initVector) {
        try {
            if (initVector == null) {
                initVector = new byte[16];
            }
            Random random = new Random();
            random.nextBytes(initVector);
            Base64.Encoder encoder = Base64.getEncoder();
            IvParameterSpec iv = new IvParameterSpec(initVector);
            SecretKeySpec skeySpec = new SecretKeySpec(key, AES);

            Cipher cipher = Cipher.getInstance(AES_SCHEMA);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value);
            return saveFile(encrypted, "jpk.zip.aes");//todo to be changed (parametrized?)
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * @return
     * @throws NoSuchAlgorithmException
     */
    private byte[] getAESEncryptionKey() throws NoSuchAlgorithmException {
        // Get the KeyGenerator
        KeyGenerator kgen = KeyGenerator.getInstance(AES);
        kgen.init(256);
        // Generate the secret key specs.
        SecretKey skey = kgen.generateKey();
        return skey.getEncoded();
    }

    /**
     * @param input
     * @return
     * @throws NoSuchAlgorithmException
     */
    private String getMD5EncodedString(byte[] input) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance(MD5);
        byte[] encodedhash = digest.digest(input);
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(encodedhash);
    }

    /**
     * @param requestNotSignedXml
     * @return
     * @throws KeyStoreException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws XAdES4jException
     * @throws TransformerException
     */
    public File signXades(File requestNotSignedXml) throws KeyStoreException, ParserConfigurationException, SAXException, IOException, XAdES4jException, TransformerException {
        KeyingDataProvider kp = new FileSystemKeyStoreKeyingDataProvider("jks",
                this.getClass().getResource("/keystore.jks").getPath(),
                list -> {
                    for (X509Certificate x509Certificate : list) {
                        int version = x509Certificate.getVersion();
                        return x509Certificate;
                    }
                    return null;
                },
                () -> "test".toCharArray(),
                (s, x509Certificate) -> "JPK".toCharArray(),
                true);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(requestNotSignedXml);

        Element elem = doc.getDocumentElement();
        DOMHelper.useIdAsXmlId(elem);

        DataObjectDesc obj = new DataObjectReference("#" + elem.getAttribute("Id"))
                .withTransform(new EnvelopedSignatureTransform());
        SignedDataObjects dataObjs = new SignedDataObjects().withSignedDataObject(obj);

        XadesSigningProfile p = new XadesBesSigningProfile(kp);
        XadesSigner signer = p.newSigner();


        new Enveloped(signer).sign(doc.getDocumentElement());
        TransformerFactory tf = TransformerFactory.newInstance();

        File outputFile = new File("request.xml");
        FileOutputStream out = new FileOutputStream(outputFile);
        tf.newTransformer().transform(
                new DOMSource(doc),
                new StreamResult(out));
        out.close();
        return outputFile;
    }

    /**
     * @param key
     * @param initVector
     * @param encrypted
     * @return
     */
    private String decrypt(String key, String initVector, String encrypted) {
        Base64.Decoder decoder = Base64.getDecoder();
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(UTF_8));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(UTF_8), AES);

            Cipher cipher = Cipher.getInstance(AES_SCHEMA);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(decoder.decode(encrypted));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * @param xmlFile
     * @return
     * @throws IOException
     */
    private File zipXmlFile(File xmlFile) throws IOException {
        byte[] buffer = new byte[1024];

        ZipOutputStream zos = null;
        FileOutputStream fos;
        File output = new File("MyFile.zip");
        fos = new FileOutputStream(output);
        zos = new ZipOutputStream(fos);
        ZipEntry ze = new ZipEntry(xmlFile.getName());
        zos.putNextEntry(ze);
        FileInputStream in = new FileInputStream(xmlFile);

        int len;
        while ((len = in.read(buffer)) > 0) {
            zos.write(buffer, 0, len);
        }

        in.close();
        zos.closeEntry();

        zos.close();
        return output;
        //remember close it
    }

    /**
     * @param fileJpk
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    private String getFileSHAHashInBase64(File fileJpk) throws NoSuchAlgorithmException, IOException {
        MessageDigest digest = MessageDigest.getInstance(SHA_256);
        byte[] fileBytes = Files.readAllBytes(fileJpk.toPath());
        byte[] encodedhash = digest.digest(fileBytes);
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(encodedhash);
    }

    /**
     * @return
     * @throws IOException
     * @throws KeyStoreException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     */
    private PublicKey loadMFCertificate() throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
        Base64.Encoder encoder = Base64.getEncoder();
        KeyStore keyStore = KeyStore.getInstance("JKS");

        InputStream readStream = new FileInputStream(new File(getClass().getResource("/keystore.jks").getPath()));
        keyStore.load(readStream, "test".toCharArray());
        PublicKey pubKey = keyStore.getCertificate("test-e-dokumenty.mf.gov.pl (certum level iv ca)").getPublicKey();
        readStream.close();
        return pubKey;
    }

    /**
     * @param data
     * @return
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws CertificateException
     * @throws KeyStoreException
     * @throws IOException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    private String rsaEncrypt(byte[] data) throws NoSuchPaddingException,
            NoSuchAlgorithmException, CertificateException, KeyStoreException,
            IOException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Base64.Encoder encoder = Base64.getEncoder();
        PublicKey pubKey = loadMFCertificate();
        Cipher cipher = Cipher.getInstance(RSA_SCHEMA);
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        byte[] cipherData = cipher.doFinal(data);
        return encoder.encodeToString(cipherData);
    }

}
