package pl.adriankozlowski.enums;

public enum FileType {
    JPK("JPK"),
    JPK_AH("JPK_AH");

    private final String text;

    /**
     * @param text
     */
    private FileType(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
