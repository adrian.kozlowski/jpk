package pl.adriankozlowski.enums;

public enum FormCodeType {
    JPK("JPK"),
    JPK_VAT("JPK_VAT");

    private final String text;

    /**
     * @param text
     */
    private FormCodeType(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
