package pl.adriankozlowski.exceptions;

public class MF500Exception extends RuntimeException {
    public MF500Exception(String s) {
        super(s);
    }
}
