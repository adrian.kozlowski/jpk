package pl.adriankozlowski.exceptions;

public class MF400Exception extends RuntimeException {
    public MF400Exception(String s) {
        super(s);
    }
}
