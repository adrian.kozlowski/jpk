package pl.adriankozlowski.exceptions;

public class CipherException extends Exception {
    public CipherException(String message, Exception e) {
        super(message, e);
    }
}
