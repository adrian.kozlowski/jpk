package pl.adriankozlowski.exceptions;

public class MFUnknownException extends RuntimeException {
    public MFUnknownException(String message) {
        super(message);
    }
}
