package pl.adriankozlowski.response;

import java.util.List;
import java.util.Map;

public class InitResponse {
    String referenceNumber;
    long timeoutInSec;
    List<UploadFile> requestToUploadFileList;

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public InitResponse setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
        return this;
    }

    public long getTimeoutInSec() {
        return timeoutInSec;
    }

    public InitResponse setTimeoutInSec(long timeoutInSec) {
        this.timeoutInSec = timeoutInSec;
        return this;
    }

    public List<UploadFile> getRequestToUploadFileList() {
        return requestToUploadFileList;
    }

    public InitResponse setRequestToUploadFileList(List<UploadFile> requestToUploadFileList) {
        this.requestToUploadFileList = requestToUploadFileList;
        return this;
    }

    public static class UploadFile {
        private String blobName;
        private String fileName;
        private String url;
        private String method;
        private String headerList;
        private Map<String, String> headers;

        public String getBlobName() {
            return blobName;
        }

        public UploadFile setBlobName(String blobName) {
            this.blobName = blobName;
            return this;
        }

        public String getFileName() {
            return fileName;
        }

        public UploadFile setFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public String getUrl() {
            return url;
        }

        public UploadFile setUrl(String url) {
            this.url = url;
            return this;
        }

        public String getMethod() {
            return method;
        }

        public UploadFile setMethod(String method) {
            this.method = method;
            return this;
        }

        public String getHeaderList() {
            return headerList;
        }

        public UploadFile setHeaderList(String headerList) {
            this.headerList = headerList;
            return this;
        }

        public Map<String, String> getHeaders() {
            return headers;
        }

        public UploadFile setHeaders(Map<String, String> headers) {
            this.headers = headers;
            return this;
        }
    }

}
