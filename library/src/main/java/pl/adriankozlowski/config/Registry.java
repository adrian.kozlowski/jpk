package pl.adriankozlowski.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Registry {

    private static Registry instance;
    private Properties properties;

    private List<RegistryListener> listeners = new ArrayList();

    private Registry() {
        properties = new Properties();
        Path path = Paths.get("jpkLibrary.properties");
        try {
            if (Files.exists(path)) {
                properties.load(Files.newBufferedReader(path));
            } else {
                setDefaultValuesProperties(properties);
                properties.store(Files.newBufferedWriter(path, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE), null);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void setDefaultValuesProperties(Properties properties) {
        properties.setProperty("schema_version", "1-0");
        properties.setProperty("aes_schema", "AES/CBC/PKCS5Padding");
        properties.setProperty("rsa_schema", "RSA/ECB/PKCS1Padding");
        properties.setProperty("version", "01.02.01.20160617");
        properties.setProperty("rsa", "RSA");
        properties.setProperty("rsa_mode", "ECB");
        properties.setProperty("rsa_padding", "PKCS#1");
        properties.setProperty("block", "16");
        properties.setProperty("aes_mode", "CBC");
        properties.setProperty("aes_padding", "PKC#7");
        properties.setProperty("aes_size", "256");
        properties.setProperty("iv_bytes", "16");
        properties.setProperty("encoding", "Base64");
        properties.setProperty("alg_md5", "MD5");
        properties.setProperty("sha_256", "SHA-256");
        properties.setProperty("zip_mode", "zip");
        properties.setProperty("zip_type", "split");
        properties.setProperty("package", "pl.adriankozlowski.jpk");
        properties.setProperty("date_format", "yyyyMMddHHmm");
        properties.setProperty("xml_ext", ".xml");
        properties.setProperty("utf_8", "UTF-8");
        properties.setProperty("aes", "AES");
        properties.setProperty("md5", "MD5");
        properties.setProperty("initRequestUrl", "https://test-e-dokumenty.mf.gov.pl/api/Storage/InitUploadSigned");
    }

    public static Registry getInstance() {
        if (instance == null)
            instance = new Registry();
        return instance;
    }

    public void addChangePropertyListener(RegistryListener listener) {
        listeners.add(listener);
    }

    public void removeChangePropertyListener(RegistryListener listener) {
        listeners.remove(listener);
    }

    public void setProperty(String key, String value) {
        String oldProperty = properties.getProperty(key);
        properties.setProperty(key, value);
        listeners.forEach(registryListener -> registryListener.onPropertyChange(key, value, oldProperty));
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

}
