package pl.adriankozlowski.config;

public interface RegistryListener {
    void onPropertyChange(String key, String newValue, String oldValue);
}
