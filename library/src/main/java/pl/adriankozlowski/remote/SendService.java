package pl.adriankozlowski.remote;

import pl.adriankozlowski.exceptions.CipherException;
import pl.adriankozlowski.response.InitResponse;
import pl.adriankozlowski.services.SendServiceImpl;

import java.io.IOException;

public interface SendService {

    default SendService getInstance() {
        return new SendServiceImpl();
    }

    InitResponse composeInitRequest(String pathToJpkXml) throws CipherException, IOException;

    void checkStatus();
}
